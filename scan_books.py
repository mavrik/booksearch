#!/usr/bin/env python3
# coding: utf-8

ROOT_DIRECTORY="/DataVolume/shares/Public/Global/"
DATABASE_FILE="/root/books.db"
FILETYPES=["mobi", "epub", "pdf", "txt", "lit", "doc", "rtf", "cbr"]

import os
import sqlite3

files_db = sqlite3.connect(DATABASE_FILE)
c = files_db.cursor()
c.execute("DROP TABLE IF EXISTS files")
c.execute("CREATE VIRTUAL TABLE files USING fts4(name text, path text, type text, date integer, filesize integer)")

for root, _, files in os.walk(ROOT_DIRECTORY):
    for fil in files:
        name, extension = os.path.splitext(fil)
        if extension[1:] in FILETYPES:
            filepath = os.path.join(root, fil)
            modified = os.path.getmtime(filepath)
            size = os.path.getsize(filepath)
            c.execute("INSERT INTO files VALUES(?, ?, ?, ?, ?)", (fil, 
                                                                  os.path.relpath(filepath, ROOT_DIRECTORY), 
                                                                  extension[1:],
                                                                  modified,
                                                                  size))
files_db.commit()
c.close()
files_db.close()
