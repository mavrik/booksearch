<?php
include("header.html");
date_default_timezone_set("Europe/Ljubljana");

function return_message($string) {
    echo('</div><div id="error-message"><p>');
    echo($string);
    echo('</p></div>');
};

function format_size($size) {
    $suffixes = array("B", "KB", "MB", "GB", "TB");

    $i = 0;
    while ($size > 1024) {
        $i++;
        $size = $size / 1024;
    }

    return number_format($size, 1) . " " . $suffixes[$i];
}

if (!isset($_GET['search_query'])) 
{
    return_message("Dude, ask me something!");
}
else
{
    $files_db = new SQLite3("/root/books.db");
    $query_str = $files_db->escapestring($_GET['search_query']);
    $results = $files_db->query("SELECT * FROM files WHERE name MATCH '{$query_str}'");

    // Prepare table header
    echo('<table><tr><th><img src="icons/pixel.gif" alt="[ICO]" width="24" height="24" /></th><th><a href="?C=N;O=D">Name</a></th><th><a href="?C=M;O=A">Last modified</a></th><th><a href="?C=S;O=A">Size</a></th></tr>');

    $count = 0;
    while ($row = $results->fetchArray()) 
    {
        $count++;
        echo('<tr><td valign="top">');
        // Add file icon
        $type = $row['type'];
        if ($type == "txt" or $type == "rtf") 
        {
            $type_ico = "text.png";
        }
        else
        {
            $type_ico = $row['type'] . ".png";
        }

        echo('<img src="icons/' . $type_ico . '" width="24" heigt="24" />');
        echo('</td>');
        // File and link
        $path_str = "../" . $row['path'];
        echo("<td><a href=\"{$path_str}\">{$row['name']}</a></td>");
        // Modifed
        $modified = date("d-M-Y H:i", $row['date']);
        echo("<td align=\"right\">{$modified}  </td>");
        // Filesize
        $size = format_size($row['filesize']);
        echo("<td align=\"right\">$size</td>");
        echo('</tr>'); 
    }

    if ($count == 0)
    {
        echo('<td colspan="4">There were no results.</td>');
    }

    echo('</table>');
}

include("footer.html");
?>
